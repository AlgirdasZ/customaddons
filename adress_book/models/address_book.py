# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AddressBook(models.Model):
    _name = 'address.book'

    name = fields.Char(string='Vardas ir pavardė')
    gender = fields.Selection(
        [('v', 'Vyras'),
         ('m', 'Moteris')]
    )
    birthday = fields.Date('Gimimo data')
    age = fields.Integer('Adresato amžius', compute='_compute_age')
    phone_nr = fields.Integer(string='Phone number')
    parent_id = fields.Many2one('address.book', string='Related Company', index=True)
    parent_name = fields.Char(related='parent_id.name', readonly=True, string='Parent name')
    child_ids = fields.One2many('address.book', 'parent_id', string='Contacts')
    relation = fields.Selection(
        [('colleague', 'Kolega'),
         ('acquaintance', 'Pazistamas'),
         ('close_one', 'Artimas'),
         ('friend', 'Draugas')]
    )



    @api.depends('birthday')
    def _compute_age(self):
        for rec in self:
            if not rec.birthday:
                rec.age = 0
                continue
            birthday = fields.Date.from_string(rec.birthday)
            today = fields.Date.from_string(fields.Date.today())
            rec.age = today.year - birthday.year - ((today.month, today.day) < (birthday.month,birthday.day))

# class Connections(AdressBook):
#     _name = "adress.book.connections"
#
#     name= fields.Char()
#     connections = fields.One2many('adress.book', string="Connections")
        # today_str = fields.Date.today()
        # today = fields.Date.from_string(today_str)
        # for rec in self:
        #     birth = fields.Date.from_string(rec.birthday)
        #
        #     rec.age = today.year - birth.year

            # return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))

    # sita klase turetu sudaryti zodynu sarasus kuriuo butu idedami vartotoju id
    # template'o laukai
    # name = fields.Char()
    # value = fields.Integer()
    # value2 = fields.Float(compute="_value_pc", store=True)
    # description = fields.Text()
    #
    # @api.depends('value')
    # def _value_pc(self):
    #     self.value2 = float(self.value) / 100

# class adress(models.Model):
#     _name = 'adress.adress'
# # planuoju tureti views matomus laukus pilno vardo, lyties, gimimo dienos ir amziaus, visi laukai ivedami isskyrus amziu
# # amzius apskaiciuojamas modelio pagal gimimo diena
#     fullname = fields.Char(required=True)
#     gender = fields.Char()
#     birthday = fields.Integer()
#     age = fields.Char(calculate_age(birthday))
#
#     def calculate_age(self):
#         today = date.today()
#         return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))
