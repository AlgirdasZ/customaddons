# -*- coding: utf-8 -*-
{
    'name': "Address Book",

    'summary': """
        customer challenge""",

    'description': """
        Meant to familiarise learner with programming in odoo framework.
        Plus, teach how to create functional tools for clients.
    """,

    'author': "Algirdas Zaborskis",
    'website': "http://www.justajoke.not",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/address_book.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    # 'demo': [
    #     'demo/demo.xml',
    # ],
}